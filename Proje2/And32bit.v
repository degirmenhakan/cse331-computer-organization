`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    17:56:17 11/22/2013 
// Design Name: 
// Module Name:    And32bit 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module And32bit(x,y,z);

	input [31:0] x;
	input [31:0] y;
	output [31:0] z;
	
	//sayilarin 16 biterlik kısımlarını alip and islemine soktum
	And16bit a16_1(x[15:0],y[15:0],z[15:0]);
	And16bit a16_2(x[31:16],y[31:16],z[31:16]);

endmodule
