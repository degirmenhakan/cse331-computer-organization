`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    18:08:36 11/22/2013 
// Design Name: 
// Module Name:    Xor32bit 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module Xor32bit(x,y,z);
	input [31:0] x;
	input [31:0] y;
	output [31:0] z;
	
	//sayilarin 16 biterlik kısımlarını alip xor islemine soktum
	Xor16bit x16_1(x[15:0],y[15:0],z[15:0]);
	Xor16bit x16_2(x[31:16],y[31:16],z[31:16]);

endmodule
