`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: Hakan DEGIRMEN
// 
// Create Date:    18:34:19 11/22/2013 
// Design Name: 
// Module Name:    Alu32bit 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////

module Alu32bit(x,y,z,select,carrayout,enable,zero,V,set);

	//input output
	input [31:0] x; //toplanan 1 
	input [31:0] y; //toplanan 2
	output [31:0] z; //toplam
	reg [31:0] z; //atama yapabilmek icin reg yapt�m
	wire [4:0] shift=4'b0; //kac shift edilecegi
	input [2:0] select; //hangi islem yap�lmak istendigi
	output carrayout; //artma var ise
	//ara kablolar
	wire [31:0] wire1; 
	wire [31:0] wire2;
	wire [31:0] wire3;
	wire [31:0] wire4;
	wire [31:0] wire5;
	wire [31:0] wire6;

	
	input enable;//alu izin
	//zero islemi breanch icin
	output zero;
	reg zero;
	output set,V;
	wire overflow;

	


		//sonuclar� hesaplay�p wire1....wire6 ya koyuyorum
		FullAdder32bit fulladder1(x[31:0],y[31:0],1'b0,wire1[31:0],carryout,overflow);

		Sub32bit sub1(x[31:0],y[31:0],1'b1,wire2[31:0],carryout);

		And32bit and1(x[31:0],y[31:0],wire3[31:0]);

		Not32bit not1(x[31:0],wire4[31:0]);

		Xor32bit xor1(x[31:0],y[31:0],wire5[31:0]);
		

		Or32bit or1(x[31:0],y[31:0],wire6[31:0]);	

		
	

		
		reg V,set;
		always @ (overflow)//secme islemi overflow belirleyecek
		begin
			if(overflow==0)//eger overflow varsa Tasma 1 yoksa 0 olacak
				V=1'b0;
			else
				V=1'b1;
		end
		
		
			//Enable ve set degeri secme islemine yard�mc� oalcak
		always@(enable or select or set)
		if(enable==1)//eneable 1 oldugunda islem yapar
		begin
			if(select[0] == 0 & select[1]==0 & select[2]==0) //select 0 ise and
				z=wire3;
			if(select[0]==1 & select[1]==0 & select[2]==0)//select 1 ise or islemi
				z= wire6;
			if(select[0]==0 & select[1]==1 & select[2]==0)//select 2 ise add islemi
					z=wire1;
					
			if(select[0]==0 & select[1]==1 & select[2]==1)//select 6 oldugunda sub islemi
				begin
					z=wire2;
				end	
			if(select[0]==1 & select[1]==1 & select[2]==0)//select 3 oldugunda xor islemi
				z=wire5;
				
			if(select[0]==1 & select[1]==1 & select[2]==1) //select 7 oldugunda set on less than islemi yapacak
			begin
				if(set==1)
					z=32'b1;
				else
					z=32'b0;
			end
				
		end	
		else//enable 1 degilse z 0 olur
			z=32'b00000000000000000000000000000000;
			
		always @ (z) //eger z 0 ise zero biti 1 degilse 0 olur
			begin
				if(z==0)
					zero=1'b1;
				else	
					zero=1'b0;
					
		end	
		
		always @(V)//eger tasma varsa set less than degeri son en yuksek degerli eleman�n ters yoksa kendisi olur
		begin
			if(V==0)
				set= z[31];
			if(V==1)
				set= ~z[31];
		end
		
endmodule