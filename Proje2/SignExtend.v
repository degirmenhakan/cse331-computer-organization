`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    23:49:40 12/26/2013 
// Design Name: 
// Module Name:    SignExtend 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module SignExtend(immed,cikti
    );

	//Sign Extend 16 bitlik say�y� isaretine gore - ise bas�na 16 tane 1 koyarak + ise 16 tane 0 koyarak 32 bit yapar
	input [15:0] immed;
	reg [31:0] cikti; //32 bitlik ciktiyi tutacak degisken
	output [31:0] cikti;
	
	always @(immed)//immediatenin ilk bitine g�re islem yapacagim
		begin
			if(immed[15]==0)// most significant biti 0 ise pozitiftir ve 
				begin
					cikti[31:16]=16'b0;//c�kt�n� ilk 16 biti 0 geri kalana da 16 bitlik say� yerlestirilir
					cikti[15:0]=immed[15:0];
				end
			else
			begin
				cikti[31:16]=16'b1111111111111111;//ilk bit 0 degilse result olarak bas�na negatif oldugu icin
				cikti[15:0]=immed[15:0];//16 bit 1 koyuyorum ve immedi. degeri ekliyorum kalan k�sma
			
			end
		end
	

endmodule
