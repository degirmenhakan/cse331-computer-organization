`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    16:52:43 12/28/2013 
// Design Name: 
// Module Name:    AdderForJump 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module AdderForJump(PcArti4,ShiftLeft,AddOut
    );
	// input - output
	 wire V;//overflow
	 wire carryOut;
	 input [31:0] PcArti4;
	 input [31:0] ShiftLeft;
	 output [31:0] AddOut;
	 wire cin=1'b0;//left den gelen deri 
	 FullAdder32bit AddShiftPc(PcArti4[31:0],ShiftLeft[31:0],cin,AddOut[31:0],carryOut,V); //pcArti 4 e shiftLefte gelen degeri topluyorum
	 


endmodule
