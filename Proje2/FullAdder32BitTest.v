`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   22:48:56 12/25/2013
// Design Name:   FullAdder32bit
// Module Name:   C:/Users/Hakan/Downloads/Proje2/FullAdder32BitTest.v
// Project Name:  Proje2
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: FullAdder32bit
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module FullAdder32BitTest;

	// Inputs
	reg [31:0] x;
	reg [31:0] y;
	reg cin;

	// Outputs
	wire [31:0] Sum;
	wire cout1;

	// Instantiate the Unit Under Test (UUT)
	FullAdder32bit uut (
		.x(x), 
		.y(y), 
		.cin(cin), 
		.Sum(Sum), 
		.cout1(cout1)
	);

	initial begin
		// Initialize Inputs
		x = 0;
		y = 0;
		cin = 0;

		// Wait 100 ns for global reset to finish
		#100;
		x=32'b01;
		y=32'b00;
		cin=0;
        
		// Add stimulus here

	end
      
endmodule

