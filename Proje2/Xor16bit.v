`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    18:07:42 11/22/2013 
// Design Name: 
// Module Name:    Xor16bit 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module Xor16bit(x,y,z);

	input [15:0] x;
	input [15:0] y;
	output [15:0] z;
	
	//4 er bit xor islemine gidip z ye c�kt� veriyorum
	Xor4bit x4_1(x[3:0],y[3:0],z[3:0]);
	Xor4bit x4_2(x[7:4],y[7:4],z[7:4]);
	Xor4bit x4_3(x[11:8],y[11:8],z[11:8]);
	Xor4bit x4_4(x[15:8],y[15:8],z[15:12]);

endmodule
