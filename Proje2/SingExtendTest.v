`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   23:57:03 12/26/2013
// Design Name:   SignExtend
// Module Name:   C:/Users/Hakan/Downloads/Proje2/SingExtendTest.v
// Project Name:  Proje2
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: SignExtend
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module SingExtendTest;

	// Inputs
	reg [15:0] immed;

	// Outputs
	wire [31:0] cikti;

	// Instantiate the Unit Under Test (UUT)
	SignExtend uut (
		.immed(immed), 
		.cikti(cikti)
	);

	initial begin
		// Initialize Inputs
		immed = 16'b1111111111111000;

		// Wait 100 ns for global reset to finish
		#100;
        
		// Add stimulus here

	end
      
endmodule

