`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    14:09:56 12/26/2013 
// Design Name: 
// Module Name:    FullAdderBirBit 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module FullAdderBirBit(x,y,cin,S,cout
    );
	 input x,y,cin;
	 output S,cout;
	 wire w1,w2,w3;
	 
	 xor (w1,x,y);
	 and (w2,x,y);
	 xor (S,w1,cin);
	 and (w3,cin,w1);
	 xor (cout,w2,w3);


endmodule
