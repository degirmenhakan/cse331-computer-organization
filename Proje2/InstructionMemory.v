`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    01:51:43 12/28/2013 
// Design Name: 
// Module Name:    InstructionMemory 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////////
//		Hocam testi yapmad�m.Schematic denedi�imde anlamad���m bi nedenden duzgun 
// c�kt� alam�yorum.Tasar�ma tamamen uydum.Butun islemler tasar�ma uygun oldugunu
//dusunuyorum.Eski cal��malar�n �zerine d�zeltmeler yaparak olusturdum.	         																			
//////////////////////////////////////////////////////////////////////////////////



////////////////////////////////////////////////////////////////////////////////
//		Hakan DEGIRMEN NO:101044057	DATE:28.12.2013			
////////////////////////////////////////////////////////////////////////////////

module InstructionMemory(pc,instruction
    );
	input [31:0] pc;
	
	
	output [31:0]instruction;
	reg [31:0] hafiza [0:31];
	assign instruction =  hafiza[pc];
	
	initial begin//dosyadan okuma instructionu dosyadan okuyorum
	$readmemb("instructionMem.mem",hafiza);
	end
	 
	wire [31:0]pcArti4;//bir sonraki islemi al�yorum
	
	AdderForPc addforpc(pc, pcArti4);//bir sonraki instruction yukeniyor
	
	
	
	wire RegDes,Jump,Branch,MemRead,MemtoReg,MemWrite,AluSrc,RegWrite;
	wire [1:0] Aluop;
	
	 ControlUnit   ContUnit(instruction[31:26] ,RegDes,Jump,Branch,MemRead,MemtoReg,Aluop,MemWrite,AluSrc,RegWrite
    );//instructionun ilk 6 bitini control unite gonderiyorm
	
	
	 //register islemleri 
	 wire [31:0]regicerik1;//1.registerin icerigi buraya gelecek
	 wire [31:0] regicerik2;//2.registerin icerigi buraya gelecek
	// input [4:0] writeRegAdres;//yazma islemi varsa hangi registara yaz�lacag�n� tutan adres
	 reg [31:0] writeIcerik;//registera yazma varsa yaz�lacak deger
	 reg [4:0] writeRegAdres;
	 //yaz�lacak register adresini belirleyen mux Select:RegDest
	 always @(RegDes)
	 begin 
		if(RegDes==1)
			  writeRegAdres = instruction[15:11];
		if(RegDes==0)
			 writeRegAdres = instruction[20:16];
	 end
	 
	 
	 Register    registerIslem(instruction[25:21],instruction[20:16],regicerik1,regicerik2,RegWrite,writeRegAdres,writeIcerik
    );
	 
	 /////////////Sing Extend islemi
	 wire [31:0] SingExtendOutput;//16 bitlik sayiyi isaretli bicimde 32 bite ceviriyor
	 SignExtend signExtendIslem(instruction[15:0],SingExtendOutput);
	 
	 ///////////////////////ALuControl islemi
	 wire [2:0] AluSec;
	 AluControl(instruction[5:0],Aluop[1:0],AluSec[2:0]
    );
	 
	 
	 /*******ALu islemleri*********/
	reg [31:0] AluData1;
	
	reg [31:0]AluData2;
	wire [31:0]AluOut;
	wire [4:0] shift;
	wire carryout,zero,V,set;
	
	///////////Alu Mux
	always @(AluSrc)
	begin
		AluData1=regicerik1;
		if(AluSrc==1)
			AluData2=SingExtendOutput;
		if(AluSrc==0)
			AluData2=regicerik2;
	end
	Alu32bit AluIslem(AluData1,AluData2,AluOut,AluSec[2:0],carrayout,1'b1,zero,V,set);
	
	
	///////////Ram islemi
	wire [31:0] MemoryCikti;
	Ram512mb RamIslemi(MemRead,AluOut[31:0],MemoryCikti[31:0],regicerik2[31:0],MemWrite);
	
	//Ram Mux Memorye yazmak istememe durumu icin
	
	//reg [31:0] writeIcerik;
	always @(MemtoReg)
	begin
		if(MemtoReg==1)
			writeIcerik = MemoryCikti;
		if(MemtoReg==0)
			writeIcerik = AluOut;
	end
	
	//////////Jump islemi
	wire BrancIslemi;
	assign BranchIslemi= zero & Branch;
	wire [31:0] ShiftLeft2IslemiCikti;
	wire [31:0] PcforJump;
	ShiftLeft2 ShiftLeftIslemi(SingExtendOutput[31:0],ShiftLeft2IslemiCikti[31:0]);
	AdderForJump addPcShiftLeft2(ShiftLeft2IslemiCikti[31:0],pcArti4[31:0],PcforJump[31:0]);
	reg [31:0] Yenipc;
	////////Jump Mux
	always @(BrancIslemi)
	begin
		if(BrancIslemi==0)
			Yenipc = pcArti4;
		if(BrancIslemi==1)
			Yenipc=ShiftLeft2IslemiCikti;
	end
	
		
endmodule
