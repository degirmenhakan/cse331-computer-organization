`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    01:30:40 12/28/2013 
// Design Name: 
// Module Name:    AdderForPc 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module AdderForPc(pcDegeri,pcOutput
    );
	 input [31:0] pcDegeri;//32 bitlik pc deki degeri al�yorum
	 output [31:0] pcOutput;//pc ye 4 eklenmis hali
	 reg [31:0] pcArti4=32'b00000000000000000000000000000100;//4 degeri
	 wire carryOut,V;//artan ve overflow durumlar� icin degiskenler
	 FullAdder32bit  f1PcArti4(pcDegeri[31:0],pcArti4[31:0],1'b0,pcOutput[31:0],carryOut,V);//islem yap�l�p pcouta pc nin 4 fazlas� konulur

endmodule
