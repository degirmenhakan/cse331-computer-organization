`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    18:01:32 11/22/2013 
// Design Name: 
// Module Name:    Or4bit 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module Or4bit(x,y,z);
	//input output
	input [3:0] x;
	input [3:0] y;
	output [3:0] z;
	//her sayiyi bit bit or islemine sokuyoruz
	Or1bit o1(x[0],y[0],z[0]);
	Or1bit o2(x[1],y[1],z[1]);
	Or1bit o3(x[2],y[2],z[2]);
	Or1bit o4(x[3],y[3],z[3]);


endmodule
