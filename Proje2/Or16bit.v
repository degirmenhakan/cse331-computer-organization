`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    18:02:36 11/22/2013 
// Design Name: 
// Module Name:    Or16bit 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module Or16bit(x,y,z);

	input [15:0] x;
	input [15:0] y;
	output [15:0] z;
	
	//4 er bit or islemine gidip z ye c�kt� veriyorum
	Or4bit o4_1(x[3:0],y[3:0],z[3:0]);
	Or4bit o4_2(x[7:4],y[7:4],z[7:4]);
	Or4bit o4_3(x[11:8],y[11:8],z[11:8]);
	Or4bit o4_4(x[15:8],y[15:8],z[15:12]);

endmodule
