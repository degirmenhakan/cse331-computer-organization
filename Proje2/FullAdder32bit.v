`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    17:11:07 11/22/2013 
// Design Name: 
// Module Name:    FullAdder32bit 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module FullAdder32bit(x,y,cin,Sum,cout1,V);
	//32 bitlik input ve outputumu tan�ml�yorum
	input [31:0] x;
	input [31:0] y;
	output[31:0] Sum;
	wire [30:0] overflow; 
	output V;//overflow varsa 1 yoksa 0 olacak
	//ilk carry toplama icin 0 cikarma icin 1 olacak
	input cin;
	output cout1;//32 bitlik toplamdan tasan bit
	wire [2:0] cout;
	
	//iki adet 16 bitlik sayiyi toplama blogu ile 32 bitlik sayimi toplaya bilirim
	FullAdder16bit f1(x[15:0],y[15:0],cin,Sum[15:0],overflow[15:0],cout[0]);
	FullAdder16bit f2(x[31:16],y[31:16],cout[0],Sum[31:16],overflow[30:15],cout[1]);
	assign  cout1=cout[1];//artan k�s�m
	assign V = (overflow[30] ^ overflow[29]);//32 bitte tasan bit
	
endmodule
