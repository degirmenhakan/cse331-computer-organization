`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    17:18:01 11/22/2013 
// Design Name: 
// Module Name:    Not16bit 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module Not16bit(x,y);
	//input output tanimlamasi
	input [15:0] x;
	output [15:0] y;
	
	//4 er bit islem yaptiriyorum
	Not4bit n4_1(x[3:0],y[3:0]);
	Not4bit n4_2(x[7:4],y[7:4]);
	Not4bit n4_3(x[11:8],y[11:8]);
	Not4bit n4_4(x[15:12],y[15:12]);

endmodule
