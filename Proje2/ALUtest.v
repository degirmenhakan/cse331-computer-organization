`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   21:40:37 11/22/2013
// Design Name:   Alu32bit
// Module Name:   D:/degirmen/Proje2/ALUtest.v
// Project Name:  Proje2
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: Alu32bit
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module ALUtest;

	// Inputs
	reg [31:0] x;
	reg [31:0] y;
	reg [2:0] select;
	reg [4:0] shift;

	// Outputs
	wire [31:0] z;
	wire carrayout;

	// Instantiate the Unit Under Test (UUT)
	Alu32bit uut (
		.x(x), 
		.y(y), 
		.z(z), 
		.select(select), 
		.carrayout(carrayout), 
		.shift(shift)
	);

	initial begin
		// Initialize Inputs
		x = 0;
		y = 0;
		select = 0;
		shift = 0;

		// Wait 100 ns for global reset to finish
		#100;
		
		#100;
		x=32'b000000000000000000000000000000001;//tanımlamalarimi yapıyorum
		y=32'b000000000000000000000000110011101;
      #100;
		select=3'b010;//delay veriyorum
		shift=0;
		// Add stimulus here

	end
      
endmodule

