`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    23:27:31 12/26/2013 
// Design Name: 
// Module Name:    Ram512mb 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////

module Ram512mb(readEnable,okunacakAdres,cikti,yazilacakDeger,writeEnable);
	input readEnable;//okuma iznini verecek giris
	input [31:0]okunacakAdres;//hangi adresden okuyacagn� veren giris
	wire [31:0] yazilacakAdres=26'b00000000000000000000000000;
	input [32:0] yazilacakDeger;
	input writeEnable;
		//27 lik olmas�n�n nedeni 
	output [31:0] cikti;//o adersdeki deger
	
	reg [31:0] hafiza [0:31];//32 bitlik 26 dizilik yer
	assign cikti = readEnable ? hafiza[okunacakAdres]:32'b00000000000000000000000000000000;//eger okuma aktifse hafizadaki deger
	//degilse 0 c�k�s� verilecek
	
	initial begin//dosyadan okuma
	$readmemb("ram512mb.mem",hafiza);
	end
	
	always begin @(posedge writeEnable)
		hafiza[yazilacakAdres]<=yazilacakDeger;
	end

endmodule
