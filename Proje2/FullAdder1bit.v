`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    17:01:51 11/22/2013 
// Design Name: 
// Module Name:    FullAdder1bit 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module FullAdder1bit(x,y,cin,Sum,cout);

	input x,y,cin; //input ve output degiskenlerim
	output Sum;	 
	output cout;
	wire wire1,wire2,wire3;
	
	assign wire2 =  x & y;// x ve y girislerini and islemine sokuyorum
	assign wire1 =  x ^ y ;//x ve y girislerini xor islemine sokuyorum
	assign Sum =   wire1 ^ cin; //(x &y ) ^cin islemi
	assign wire3 = cin & wire1; // (x&y) &cin islemi
	assign cout = wire2 & wire3;
endmodule
