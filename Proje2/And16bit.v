`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    17:53:23 11/22/2013 
// Design Name: 
// Module Name:    And16bit 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module And16bit(x,y,z);

	input [15:0] x;
	input [15:0] y;
	output [15:0] z;
	
	//4 er bit and islemine gidip z ye c�kt� veriyorum
	And4bit a4_1(x[3:0],y[3:0],z[3:0]);
	And4bit a4_2(x[7:4],y[7:4],z[7:4]);
	And4bit a4_3(x[11:8],y[11:8],z[11:8]);
	And4bit a4_4(x[15:8],y[15:8],z[15:12]);

endmodule
