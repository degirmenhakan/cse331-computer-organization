`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    17:04:37 11/22/2013 
// Design Name: 
// Module Name:    FullAdder4bit 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module FullAdder4bit(x,y,cin,Sum,cout1,out);

//input output degiskenlerimi tanitiyorum
input [3:0] x;
input [3:0] y;
input cin;
output [3:0] out;
output [3:0] Sum ;

wire [3:0] p;//carry lookahead icin kullanacagiz x+y
wire [3:0] g;//carry lookahead icin kullanacagiz xy
wire [2:0] cout;//her toplama isleminden  tasan bit


output cout1;//4 bitlik islemden tasan bit

//her biti sirasi ile toplama islemine yolluyorum ve artan sayyiyi yine input 
//bitlerle buluyorum p ve g ye kaydedip s�radaki cin i buluyorum
FullAdderBirBit f1(x[0],y[0],cin,Sum[0],out[0]);
assign p[0] = x[0] | y[0];
assign g[0] = x[0] & y[0];
assign cout[0]=g[0] | p[0] & cin; 

FullAdderBirBit f2(x[1],y[1],cout[0],Sum[1],out[1]);
assign p[1] = x[1] | y[1];
assign g[1] = x[1] & y[1];
assign cout[1]=g[1] | p[1] & cout[0]; 

FullAdderBirBit f3(x[2],y[2],cout[1],Sum[2],out[2]);
assign p[2] = x[2] | y[2];
assign g[2] = x[2] & y[2];
assign cout[2]=g[2] | p[2] & cout[1]; 
FullAdderBirBit f4(x[3],y[3],cout[2],Sum[3],out[3]);
assign p[3] = x[3] | y[3];
assign g[3] = x[3] & y[3];
assign cout1=g[3] | p[3] & cout[2]; 

endmodule

