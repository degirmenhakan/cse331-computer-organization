`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    17:09:22 11/22/2013 
// Design Name: 
// Module Name:    FullAdder16bit 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module FullAdder16bit(x,y,cin,Sum,,cout1,overflow);
	//input ve outputlari tanimliyorum
	input [15:0] x;
	input [15:0] y;
	input cin;
	output cout1;//16 bitte elde olan bit
	output [15:0] Sum ;//16 bitlik toplam
	output [11:0] overflow; 
	wire [3:0] cout;
	//4 adet 4 bitlik sayi serimi toplay�nca 16 bitlik
	//iki sayiyi toplayabilmis olacag�im
	FullAdder4bit f1(x[3:0],y[3:0],cin,Sum[3:0],overflow[2:0],cout[0]);
	FullAdder4bit f2(x[7:4],y[7:4],cout[0],Sum[7:4],overflow[5:3],cout[1]);
	FullAdder4bit f3(x[11:8],y[11:8],cout[1],Sum[11:8],overflow[8:6],cout[2]);
	FullAdder4bit f4(x[15:12],y[15:12],cout[2],Sum[15:12],overflow[11:9],cout[3]);
	assign cout1= cout[3];//16 bitlik toplamdan tasan sayi


endmodule
