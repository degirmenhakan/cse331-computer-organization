`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    17:15:43 11/22/2013 
// Design Name: 
// Module Name:    Not4bit 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module Not4bit(x,y);

	//input output degiskenerim
	input [3:0] x;
	output [3:0] y;
	
	//4 bitin not halini ald�m
	Not1bit n1(x[0],y[0]);
	Not1bit n2(x[1],y[1]);
	Not1bit n3(x[2],y[2]);
	Not1bit n4(x[3],y[3]);
	
	
endmodule
