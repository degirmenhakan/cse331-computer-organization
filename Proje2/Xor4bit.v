`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    18:05:51 11/22/2013 
// Design Name: 
// Module Name:    Xor4bit 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module Xor4bit(x,y,z);

//input output bitler
	input [3:0]x;
	input [3:0]y;
	output [3:0]z;
	
	//islemler her bit icin
	Xor1bit x1(x[0],y[0],z[0]);
	Xor1bit x2(x[1],y[1],z[1]);
	Xor1bit x3(x[2],y[2],z[2]);
	Xor1bit x4(x[3],y[3],z[3]);


endmodule
