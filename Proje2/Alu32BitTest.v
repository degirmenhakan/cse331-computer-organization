`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   18:15:43 12/26/2013
// Design Name:   Alu32bit
// Module Name:   C:/Users/Hakan/Downloads/Proje2/Alu32bitTest.v
// Project Name:  Proje2
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: Alu32bit
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module Alu32bitTest;

	// Inputs
	reg [31:0] x;
	reg [31:0] y;
	reg [2:0] select;
	reg [4:0] shift;
	reg enable;

	// Outputs
	wire [31:0] z;
	wire carrayout;

	// Instantiate the Unit Under Test (UUT)
	Alu32bit uut (
		.x(x), 
		.y(y), 
		.z(z), 
		.select(select), 
		.carrayout(carrayout), 
		.shift(shift), 
		.enable(enable)
	);

	initial begin
		// Initialize Inputs
		x = 0;
		y = 0;
		select = 0;
		shift = 0;
		enable = 0;

		// Wait 100 ns for global reset to finish
		#100;
		x=32'b01;
      y=32'b00;
		enable=1;
		select=3'b011;
		shift=0;
		// Add stimulus here

	end
      
endmodule

