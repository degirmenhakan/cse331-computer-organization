`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    01:41:17 12/27/2013 
// Design Name: 
// Module Name:    ShiftLeft2 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module ShiftLeft2(in,out
    );
	 input [31:0] in;
	 output [31:0] out;
	 reg [31:0]out;
	 reg atamaYap=1'b1;//atama yapt�g�mda s�k�nt� yasad�g�m �c�n kulland�m
	 always @(atamaYap)
	 begin
		if(atamaYap==1)
		begin//iki basamak ileri kayd�rd�m
			out[1:0]=2'b00;
			out[31:2]=in[31:2];
		end
	 end	

endmodule
