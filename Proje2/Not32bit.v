`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    17:47:54 11/22/2013 
// Design Name: 
// Module Name:    Not32bit 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module Not32bit(x,y);
	input [31:0] x;
	output [31:0] y;
	Not16bit n16_1(x[15:0],y[15:0]);
	Not16bit n16_2(x[31:16],y[31:16]);

endmodule
