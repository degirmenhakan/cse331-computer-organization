`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    18:21:57 12/27/2013 
// Design Name: 
// Module Name:    ControlUnit 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module ControlUnit(inst ,RegDes,Jump,Branch,MemRead,MemtoReg,Aluop,MemWrite,AluSrc,RegWrite
    );
	 input [5:0] inst;//6 bitlik deger instructionun ilk 6 biti icin
	 output RegDes,Jump,Branch,MemRead,MemtoReg,MemWrite,AluSrc,RegWrite;//control unit c�kt�lar�
	 output [1:0] Aluop;//2 bitlik aluop control unit c�kt�s�
	 wire Nop0,Nop1,Nop2,Nop3,Nop4,Nop5;//her poryun degilini almak icin kullanacam
	 
	//her girdinin not�n� al�yorum
	not g1(Nop0,inst[0]);
	not g2(Nop1,inst[1]);
	not g3(Nop2,inst[2]);
	not g4(Nop3,inst[3]);
	not g5(Nop4,inst[4]);
	not g6(Nop5,inst[5]);
	 
	assign RegDes = Nop0 & Nop1 & Nop2 & Nop3 & Nop4 & Nop4; //register destination hesaplan�r
	 
	 /*Ara ciktilar islemler icin*/
	wire wireLw,wireSw;
	wire wireBeq;
	 
	 /*Conrtrol unite girilen 6 bit say�ya gore ara ��kt�lar hesaplan�yor*/
	assign wireLw = inst[5] & Nop4 & Nop3 & Nop2 & Nop1 & inst[0];
	assign wireSw = inst[5] & Nop4 & inst[3] & Nop2 &inst[1] & inst[0];
	assign wireBeq= Nop5 & Nop4 & Nop3 & inst[2] &Nop1 & Nop0;
	  
	 /*Ara c�kt�lar ile ana c�kt�lar hesaplan�yor*/ 
	assign AluSrc = wireLw | wireSw;
	assign MemtoReg = wireLw;
	assign RegWrite = RegDes | wireLw;
	assign MemRead = wireLw;
	assign MemWrite = wireSw;
	assign Aluop[0] = wireBeq;
	assign Aluop[1] = RegDes;
	assign Branch =wireBeq;
	 

endmodule
