`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    17:51:01 11/22/2013 
// Design Name: 
// Module Name:    And4bit 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module And4bit(x,y,z);
	//input output
	input [3:0] x;
	input [3:0] y;
	output [3:0] z;
	//her sayiyi bit bit and islemine sokuyoruz
	And1bit a1(x[0],y[0],z[0]);
	And1bit a2(x[1],y[1],z[1]);
	And1bit a3(x[2],y[2],z[2]);
	And1bit a4(x[3],y[3],z[3]);

endmodule
