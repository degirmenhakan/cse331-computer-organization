`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    18:10:39 11/22/2013 
// Design Name: 
// Module Name:    Sub32bit 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module Sub32bit(x,y,ci,z,cout);

	input [31:0] x;
	input [31:0] y;
	input ci;
	output [31:0] z;
	output cout;
	
	wire [31:0] t;//y nin ones complimenti xor yapacag�m
	wire wire1=32'b1111111111111111111111111111111;
	wire wire2=32'b0000000000000000000000000000001;//y nin teos complimentini almak icin 1 ile toplayacag�m
	wire[31:0]t2;
	wire wire_cout;
	
	Xor32bit sub_x1(t,y,wire1);//y nin ones complimentini ald�m
	FullAdder32bit sub_full(t,wire2,1'b0,t2,wire_cout);//1 ekleyip twos complimentini ald�m
	FullAdder32bit sub_add(x,t2,1'b1,z,cout);
	 


endmodule
