`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    15:52:24 12/27/2013 
// Design Name: 
// Module Name:    AluControl 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module AluControl(inst,aluop,alusec
    );
	 input [5:0] inst;//registerin son 6 biti
	 input [1:0] aluop;//control unitten gelen iki bit
	 output [2:0] alusec;//iki girdiyee g�re alu selecti
	 reg [2:0] alusec;
	 
	always @(aluop or inst)//aluop ve ibst e gore secme yapacag�m
	begin
		if(aluop[0]==0 & aluop[1]==0)//eger aluop 0 ise
			alusec=3'b010;//alu sec 2 olacak adder
		if(aluop[0]==1 & aluop[1]==0)//eger alu op 1 ise
			alusec=3'b110; //alu sec 6 olarak degistiryorum
		if(aluop[0]==0 & aluop[1]==1)//eger aluop 2 ise
			begin
				if(inst==6'b100000) //inst 32 ise alu sec 2 olacak
				alusec=3'b010;
				if(inst==6'b100010) //inst 34 ise alusec 6 olacak
					alusec=3'b110;
				if(inst == 6'b100100) //eger inst 36ise
					alusec=3'b000;	//alusec 0 olarak loader vs.
				if(inst==6'b100101) //eger aluop 37 ise 
					alusec=3'b001;	//alusec 1 olacak
				if(inst==6'b101010) //aluop  42 ise alusec 7 olacak
					alusec=3'b111;
			end
	
	
	end


endmodule
