`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   23:43:14 12/06/2013
// Design Name:   Rom512mb
// Module Name:   D:/XilinxProjeler/Proje3Rom512mb/romtest.v
// Project Name:  Proje3Rom512mb
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: Rom512mb
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module romtest;

	// Inputs
	reg readEnable;
	reg [26:0] okunacakAdres;

	// Outputs
	wire [31:0] cikti;

	// Instantiate the Unit Under Test (UUT)
	Rom512mb uut (
		.readEnable(readEnable), 
		.okunacakAdres(okunacakAdres), 
		.cikti(cikti)
	);

	initial begin
		// Initialize Inputs
		readEnable = 0;
		okunacakAdres = 0;

		// Wait 100 ns for global reset to finish
		#100;
        
		readEnable=1'b1;//okuma izin verildi
		okunacakAdres=27'b00000000000000000000000001;//1. adresden oku
	end
      
endmodule

