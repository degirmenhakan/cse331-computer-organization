`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    22:35:46 12/06/2013 
// Design Name: 
// Module Name:    Rom512mb 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module Rom512mb(readEnable,okunacakAdres,cikti);
	input readEnable;//okuma iznini verecek giris
	input [26:0]okunacakAdres;//hangi adresden okuyacagn� veren giris
		//27 lik olmas�n�n nedeni 
	output [31:0] cikti;//o adersdeki deger
	
	reg [31:0] hafiza [0:25];//32 bitlik 26 dizilik yer
	assign cikti = readEnable ? hafiza[okunacakAdres]:32'b00000000000000000000000000000000;//eger okuma aktifse hafizadaki deger
	//degilse 0 c�k�s� verilecek

	initial begin//dosyadan okuma
	$readmemb("rom32bit.mem",hafiza);
	end

endmodule
