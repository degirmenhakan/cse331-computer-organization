`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   00:09:36 12/07/2013
// Design Name:   Rom512mb
// Module Name:   D:/XilinxProjeler/101044057_Hakan_DEGIRMEN_PROJE3/Proje3Rom512mb/TestFix.v
// Project Name:  Proje3Rom512mb
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: Rom512mb
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module TestFix;

	// Inputs
	reg readEnable;
	reg [26:0] okunacakAdres;

	// Outputs
	wire [31:0] cikti;

	// Instantiate the Unit Under Test (UUT)
	Rom512mb uut (
		.readEnable(readEnable), 
		.okunacakAdres(okunacakAdres), 
		.cikti(cikti)
	);

	initial begin
		// Initialize Inputs
		readEnable = 1;
		okunacakAdres = 0;

		// Wait 100 ns for global reset to finish
		#100;
		readEnable =1;
		okunacakAdres=26'b011;
        
		// Add stimulus here

	end
      
endmodule

