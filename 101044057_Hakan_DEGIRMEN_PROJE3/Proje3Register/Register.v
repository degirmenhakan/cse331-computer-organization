`timescale 1ns / 1ps
////////////////////////////////////////////////////////////
//					Hakan DEGIRMEN 101044057
////////////////////////////////////////////////////////////

//Bu pogram 32 bitlik 32 tane registerdan olusan bir modul calidir.
//3 adet register adres girisi verilir.
//bunlardan ikisi 2 registerdan deger okumak icin digeri yazma islemi varsa yazılacak registeri gostermek icindir
//bir adet writeEnable girisi vardır Bu giris registera yazma iznini gosterir
//diger bir giris 32 bitlik bir datadır buda eger yazma islemi varsa ne yazılacagını gosterir
//output olarak da 2 adet 32 bitlik registerlardan okunan degerler icin outputtur

module Register(regAdres1,regAdres2,regicerik1,regicerik2,writeEnable,writeRegAdres,writeIcerik
    );
	 
	 
	 //-----------input output tanımlamalari--------------------//
	 
	 input [4:0] regAdres1;//hangi registerin icerigi okunacagını belirliyor
								//32 register olabilecegi icin 5 bitlik input bize yetecektir
    input [4:0] regAdres2;//secilecek olan ikinci registeri belirleyecek
	 input writeEnable;//Registera yazılıp yazılmayacagına karar verecek
	 output [31:0] regicerik1;//regAdres1 de gösterilen  registarın icerigi 32 bit
	 output [31:0] regicerik2;//reAdres2 de gösterilen registarın icerigi 32 bit
	 input [4:0] writeRegAdres;//eger yazma islemi varsa hangi registara yazılacagını gösteriyor
	 input [31:0] writeIcerik;//Yazma islemi varsa input olarak verilen register adresine yazacak

	reg [31:0] registerDizisi [0:31];//32 dizi register degeri tutacak
	
	//---------------Okunacak dataları verilen adreslerdeki registerdan atıyorum---------------//
	assign regicerik1 = registerDizisi[regAdres1];//verilen adresdeki register icerigini regicerik1 e koyacak
	assign regicerik2 = registerDizisi[regAdres2];//verilen adresdeki registar icerigini regicerik2 e koyacak
	
	initial begin
	$readmemb("reginput.mem",registerDizisi);// reginput.mem dosyasindan degerleri cekiyor
	end
	//----------------------------eger yazma islemi varsa istenilen registera yaziyorum------------//
	always begin @ (posedge writeEnable)//yazma izni oldugunda
		registerDizisi[writeRegAdres] <= writeIcerik;//eger writeEnable 1 ise verilen icerik regiter dizinin verilen adresine yazilacak
	end
endmodule
