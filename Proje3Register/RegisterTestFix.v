`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   22:29:15 12/06/2013
// Design Name:   Register
// Module Name:   D:/XilinxProjeler/Proje3Register/RegisterTestFix.v
// Project Name:  Proje3Register
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: Register
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module RegisterTestFix;

	// Inputs
	reg [4:0] regAdres1;
	reg [4:0] regAdres2;
	reg writeEnable;
	reg [4:0] writeRegAdres;
	reg [31:0] writeIcerik;

	// Outputs
	wire [31:0] regicerik1;
	wire [31:0] regicerik2;

	// Instantiate the Unit Under Test (UUT)
	Register uut (
		.regAdres1(regAdres1), 
		.regAdres2(regAdres2), 
		.regicerik1(regicerik1), 
		.regicerik2(regicerik2), 
		.writeEnable(writeEnable), 
		.writeRegAdres(writeRegAdres), 
		.writeIcerik(writeIcerik)
	);

	initial begin
		// Initialize Inputs
		regAdres1 = 0;
		regAdres2 = 0;
		writeEnable = 0;
		writeRegAdres = 0;
		writeIcerik = 0;

		// Wait 100 ns for global reset to finish
		#100;
        
		// Add stimulus here
		
	
		writeEnable = 1'b1;
		writeRegAdres = 5'b00000;
		writeIcerik =32'b10101010101010101010101010101010;
		regAdres1= 5'b00111;
		regAdres2= 5'b00001;
		

	end
      
endmodule

